<?php
/**
 * @file
 * Contains the default display plugin.
 */

/**
 * A plugin to handle defaults on a view.
 *
 * @ingroup views_display_plugins
 */
class views_php_array_plugin_display_php_array extends views_plugin_display {

  function has_path() { return FALSE; }

  function uses_breadcrumb() { return FALSE; }

  function use_pager() { return TRUE; }

  function use_ajax() { return FALSE; }

  function use_more() { return FALSE; }

  function uses_exposed() { return FALSE; }

  function uses_fields() { return TRUE; }

  function uses_link_display() { return FALSE; }

  function accept_attachments() { return FALSE; }

  /**
   * Displays can require a certain type of style plugin. By default, they will
   * be 'normal'.
   */
  function get_style_type() {
    return 'php';
  }

  /**
   * PHP Array option definitions.
   */
  function option_definition() {
    $options = parent::option_definition();

    // Overrides standard option definition
    $options['style_plugin']['default'] = 'php_array';
    $options['sitename_title']['default'] = FALSE;

    $options['defaults']['default']['style_plugin'] = FALSE;
    $options['defaults']['default']['style_options'] = FALSE;
    $options['defaults']['default']['row_plugin'] = FALSE;
    $options['defaults']['default']['row_options'] = FALSE;
  
    return $options;
  }

  /**
   * Execute our display plugin.
   */
  function execute() {
    // We do nothing but render.
    return $this->view->render($this->display->id);
  }

  /**
   * Render the results.
   */
  function render() {
    // Delegate the rendering to the Style Plugin
    return $this->view->style_plugin->render($this->view->result);
  }

  /**
   * Called for live preview when in views UI, or as a quick
   * way of getting the results of the view back in code.
   */
  function preview() {
    if (!empty($this->view->live_preview)) {
      // Pretty print the PHP Array in Live Preview
      if (function_exists('kprint_r')) {
        return kprint_r($this->view->render(), TRUE);
      }
      else {
        return t('Enable the <em>Devel</em> module to see a live preview of this PHP Array display.');
      }
    }
    return $this->view->render();
  }
}
