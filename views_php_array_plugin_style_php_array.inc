<?php
/**
 * @file
 * Contains the PHP Array style plugin.
 */

/**
 * Default style plugin to render an RSS feed.
 *
 * @ingroup views_style_plugins
 */

class views_php_array_plugin_style_php_array extends views_plugin_style {

  function option_definition() {
    $options = parent::option_definition();
    
    // Allow fields to return arrays
    $options['php_array_fields_as_arrays'] = array(
      'default' => FALSE,
    );

    // By default the array is keyed primarily by grouping.
    // This setting ignores this key if it is not set.
    $options['php_array_construction'] = array(
      'default' => 'default',
    );

    // By default the array is keyed primarily by grouping.
    // This setting ignores this key if it is not set.
    $options['ignore_grouping_when_not_set'] = array(
      'default' => TRUE,
    );

    // By default the key of each group is "<label>: <value>".
    // This setting will exclude the label.
    $options['ignore_label_in_grouping_key'] = array(
      'default' => TRUE,
    );

    // By default (within each row) each rendered field value will be keyed
    // by the db column name. This setting causes us instead to key each
    // field value by the field label configured in the views UI.
    $options['key_fields_by_label'] = array(
      'default' => TRUE,
    );

    // Simple Shallow Array Construction
    $options['simple_shallow_key_field'] = array(
      'default' => NULL,
    );
    $options['simple_shallow_value_field'] = array(
      'default' => NULL,
    );
    $options['simple_shallow_duplicate_key_behaviour'] = array(
      'default' => 'override',
    );
    $options['simple_shallow_concatenate_separator'] = array(
      'default' => ',',
    );

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['php_array_fields_as_arrays'] = array(
      '#type' => 'checkbox',
      '#title' => t("Make all field values arrays"),
      '#description' => t("This disregards any field-level theming, and ensures that each field value is an array. Fields whose multiple values are being rendered in the same row will have each value as a separate element of this array (Any separator string will be ignored)."),
      '#default_value' => $this->options['php_array_fields_as_arrays'],
    );

    $form['php_array_construction'] = array(
      '#type' => 'radios',
      '#title' => t('Array construction'),
      '#options' => array(
        'default' => t('Normal (Groups, rows, fields)'),
        'simple_shallow' => t('Simple shallow array (key => value)'),
      ),
      '#default_value' => $this->options['php_array_construction'],
      '#weight' => -1,
    );

    $form['grouping']['#states']['visible'][':input[name="style_options[php_array_construction]"]'] = array(
      'value' => 'default',
    );

    $form['ignore_grouping_when_not_set'] = array(
      '#type' => 'checkbox',
      '#title' => t("Ignore grouping (when not set)"),
      '#description' => t("By default, when there is no grouping field, the first level of the return array will be keyed by the empty string. This setting will remove this first level, returning instead all rows directly."),
      '#default_value' => $this->options['ignore_grouping_when_not_set'],
      '#states' => array(
        'visible' => array(
          '#edit-style-options-grouping' => array(
            'value' => '',
          ),
          ':input[name="style_options[php_array_construction]"]' => array(
            'value' => 'default',
          )
        ),
      )
    );

    $form['ignore_label_in_grouping_key'] = array(
      '#type' => 'checkbox',
      '#title' => t("Key each group by the grouping field's value only"),
      '#description' => t("By default the label will be included, if one is configured."),
      '#default_value' => $this->options['ignore_label_in_grouping_key'],
      '#states' => array(
        'invisible' => array(
          '#edit-style-options-grouping' => array(
            'value' => '',
          )
        ),
        'visible' => array(
          ':input[name="style_options[php_array_construction]"]' => array(
            'value' => 'default',
          )
        ),
      )
    );

    $form['key_fields_by_label'] = array(
      '#type' => 'checkbox',
      '#title' => t("Key each field by label"),
      '#description' => t("By default each field will be keyed by the db column name. This setting will instead use the label (if one is configured) and will fallback to the db column (if a label is not configured)."),
      '#default_value' => $this->options['key_fields_by_label'],
      '#states' => array(
        'visible' => array(
          ':input[name="style_options[php_array_construction]"]' => array(
            'value' => 'default',
          )
        ),
      )
    );

    // Simple Shallow Array construction
    $fields = $this->display->handler->get_field_labels();
    $form['simple_shallow_key_field'] = array(
      '#type' => 'select',
      '#title' => t("Key field"),
      '#description' => t("The value for each key of the array."),
      '#default_value' => $this->options['simple_shallow_key_field'],
      '#options' => $fields,
      '#states' => array(
        'visible' => array(
          ':input[name="style_options[php_array_construction]"]' => array(
            'value' => 'simple_shallow',
          )
        ),
      )
    );
    $form['simple_shallow_value_field'] = array(
      '#type' => 'select',
      '#title' => t("Value field"),
      '#description' => t("The value for each value of the array."),
      '#default_value' => $this->options['simple_shallow_value_field'],
      '#options' => $fields,
      '#states' => array(
        'visible' => array(
          ':input[name="style_options[php_array_construction]"]' => array(
            'value' => 'simple_shallow',
          )
        ),
      )
    );
    $form['simple_shallow_duplicate_key_behaviour'] = array(
      '#type' => 'radios',
      '#title' => t('Handle duplicate keys'),
      '#description' => t('If duplicate values for the key are encountered, how should the array be constructed?'),
      '#options' => array(
        'override' => t('By overriding the value'),
        'array_merge' => t('By array-merging the values'),
        'concatenate' => t('By string-concatenation'),
      ),
      '#default_value' => $this->options['simple_shallow_duplicate_key_behaviour'],
      '#states' => array(
        'visible' => array(
          ':input[name="style_options[php_array_construction]"]' => array(
            'value' => 'simple_shallow',
          )
        ),
      )
    );
    $form['simple_shallow_concatenate_separator'] = array(
      '#type' => 'textfield',
      '#title' => t('String separator'),
      '#description' => t("If concatenating duplicate key values, this string will divide the values."),
      '#default_value' => $this->options['simple_shallow_concatenate_separator'],
      '#states' => array(
        'visible' => array(
          ':input[name="style_options[php_array_construction]"]' => array(
            'value' => 'simple_shallow',
          ),
          ':input[name="style_options[simple_shallow_duplicate_key_behaviour]"]' => array(
            'value' => 'concatenate',
          )
        ),
      )
    );

  }

  /**
   * Render the display in this style.
   */
  function render() {
    /**
     * These could/should really be plugins.
     * That way the options form and render callbacks would delegate to
     * the array construction plugin methods.
     */
    $render_method = 'php_array_render_' . $this->options['php_array_construction'];
    $output = $this->$render_method();
    return $output;
  }

  /**
   * Implementation of render for default array construction.
   */
  function php_array_render_default() {
    $output = array();

    // Group the rows according to the grouping field, if specified.
    $rendered_results = $this->render_results_array($this->view->result,
                                                    $this->options['key_fields_by_label']);

    // This allows calling functions to cheekily ignore any grouping.
    if (!empty($this->view->php_array_ignore_grouping)) {
      $output = $rendered_results;
    }

    // Render as default. Keyed by grouping, then results.
    else {
      $output = $this->render_grouping($rendered_results, $this->options['grouping']);

      if (!empty($this->options['ignore_grouping_when_not_set'])) {
        if (!$this->options['grouping']) {
          $output = $output[''];
        }
      }
    }

    return $output;
  }

  /**
   * Implementation of render for simple shallow array construction.
   */
  function php_array_render_simple_shallow() {
    $output = array();

    // Group the rows according to the grouping field, if specified.
    $rendered_results = $this->render_results_array($this->view->result, FALSE);

    $key_field = $this->options['simple_shallow_key_field'];
    $value_field = $this->options['simple_shallow_value_field'];
    foreach ($rendered_results as $index => $row) {
      if (isset($row[$key_field])) {

        $key = $row[$key_field];
        $value = isset($row[$value_field]) ? $row[$value_field] : NULL;

        // Key already exists - duplicate key handling
        if (isset($output[$row[$key_field]])) {
          switch ($this->options['simple_shallow_duplicate_key_behaviour']) {

            // Form an array of all items
            case 'array_merge':
              $output = array_merge_recursive($output, array($key => $value));
              break;

            // Concatenate all values together
            case 'concatenate':
              $output[$key] .= $this->options['simple_shallow_concatenate_separator'] . $value;
              break;

            // Just override the value
            case 'override':
            default:
              $output[$key] = $value;
          }
        }
        // Key does not already exist.
        else {
          $output[$key] = $value;
        }
      }
    }

    return $output;
  }

  /**
   * Processes a views results array and returns an equivalent but with
   * each row now rendered.
   *
   * Note, this does not override a parent method - but is our own.
   */
  protected function render_results_array($records, $key_by_label = FALSE) {
    // Make sure fields are rendered
    $this->render_fields($this->view->result);

    $rendered = array();
    $keys = array_keys($this->view->field);
    foreach ($records as $index => $row) {
      foreach ($keys as $col_name) {
        $key = $col_name;
        if (!empty($key_by_label)) {
          if ($this->view->field[$col_name]->options['label']) {
            $key = $this->view->field[$col_name]->options['label'];
          }
        }
        $rendered[$index][$key] = $this->get_field($index, $col_name);
      }
    }
    return $rendered;
  }

  /**
   * Modified to honour our 'ignore_label_in_grouping_key' option.
   */
  function render_grouping($records, $grouping_field = '') {
    // Make sure fields are rendered
    $this->render_fields($this->view->result);
    $sets = array();

    if ($grouping_field) {
      foreach ($records as $index => $row) {
        $grouping = '';
        // Group on the rendered version of the field, not the raw.  That way,
        // we can control any special formatting of the grouping field through
        // the admin or theme layer or anywhere else we'd like.
        if (isset($this->view->field[$grouping_field])) {
          $grouping = $this->get_field($index, $grouping_field);
          if ($this->view->field[$grouping_field]->options['label']) {
            if (empty($this->options['ignore_label_in_grouping_key'])) {
              $grouping = $this->view->field[$grouping_field]->options['label'] . ': ' . $grouping;
            }
          }
        }

        $sets[$grouping][] = $row;
      }
    }
    else {
      // Create a single group with an empty grouping field.
      $sets[''] = $records;
    }

    return $sets;
  }

  /**
   * Render all of the fields for a given style and store them on the object.
   *
   * @param $result
   *   The result array from $view->result
   */
  function render_fields($result) {
    if (!$this->uses_fields()) {
      return;
    }

    if (!isset($this->rendered_fields)) {
      $this->rendered_fields = array();
      $this->view->row_index = 0;
      $keys = array_keys($this->view->field);
      foreach ($result as $count => $row) {
        $this->view->row_index = $count;
        foreach ($keys as $id) {
          $this->rendered_fields[$count][$id] = $this->_field_theme($this->view->field[$id], $row);
        }

        $this->row_tokens[$count] = $this->view->field[$id]->get_render_tokens(array());
      }
      unset($this->view->row_index);
    }

    return $this->rendered_fields;
  }
  
  /**
   * Wrapper around field_handler::theme() that allows us to mangle around
   * with the return value.
   */
  function _field_theme($field_handler, $row) {
    
    // Do the normal thing if we aren't overriding fields
    if (empty($this->options['php_array_fields_as_arrays'])) {
      return $field_handler->theme($row);
    }
    
    // Do our own advanced render, so we can split field values up into
    // arrays.
    $output = $this->_field_advanced_render($field_handler, $row);
    if ($output === '' || $output === FALSE || is_null($output)) {
      $output = array();
    }
    elseif (!is_array($output)) {
      $output = array($output);
    }
    
    return $output;
  }

  /**
   * Render a field using advanced settings.
   *
   * This renders a field normally, then decides if render-as-link and
   * text-replacement rendering is necessary.
   *
   * @see views_handler_field::advanced_render().
   */
  function _field_advanced_render($field_handler, $values) {
    $not_render_array = $custom_array_value = FALSE;
    $last_value_string = NULL;
    if ($field_handler->allow_advanced_render() && method_exists($field_handler, 'render_item')) {
      $raw_items = $field_handler->get_items($values);
    }
    else {
      $value = $field_handler->render($values);
      if (is_array($value)) {
        $value = drupal_render($value);
      }
      $field_handler->last_render = $value;
      $field_handler->original_value = $value;
    }

    if ($field_handler->allow_advanced_render()) {
      $tokens = NULL;
      if (method_exists($field_handler, 'render_item')) {
        $items = array();
        foreach ($raw_items as $count => $item) {
          $value = $field_handler->render_item($count, $item);
          if (is_array($value)) {
            $value = drupal_render($value);
          }
          $field_handler->last_render = $value;
          $field_handler->original_value = $field_handler->last_render;

          $alter = $item + $field_handler->options['alter'];
          $items[] = $field_handler->render_text($alter);
        }
        
        // CHANGE: Explicitly render each one separately and make an array.
        $value = array();
        foreach ($items as $i) {
          $value[] = $field_handler->render_items(array($i));
          $not_render_array = TRUE;
          $custom_array_value = TRUE;
        }
        $last_value_string = $field_handler->render_items($items);
      }
      else {
        $value = $field_handler->render_text($field_handler->options['alter']);
      }

      if (!$not_render_array && is_array($value)) {
        $value = drupal_render($value);
      }
      // This happens here so that render_as_link can get the unaltered value of
      // this field as a token rather than the altered value.
      $field_handler->last_render = isset($last_value_string) ? $last_value_string : $value;
    }

    if (empty($field_handler->last_render)) {
      if (($field_handler->last_render !== 0 && $field_handler->last_render !== '0') || !empty($field_handler->options['empty_zero'])) {
        $alter = $field_handler->options['alter'];
        $alter['alter_text'] = 1;
        $alter['text'] = $field_handler->options['empty'];
        $field_handler->last_render = $field_handler->render_text($alter);
      }
    }

    return isset($custom_array_value) ? $value : $field_handler->last_render;
  }
}
